#include <stdio.h>

/* echange: échange *pa et *pb */
void echange(int* pa, int* pb)
{
    int temp = *pa;
    *pa = *pb;
    *pb = temp;
}

int main()
{
    int x = 1, y = 2;
    echange(&x, &y);
    printf("x = %i, y = %i.\n", x, y);
    return 0;
}