/* strlen: calcule la longueur de la chaine s (v2) */
int strlen(char* s)
{
	char* p;
	for ( p=s; *p; p++ )
		;
	return p-s;
}
