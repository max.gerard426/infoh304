#include <iostream>
#include <string>
#include "MembreULB.h"
using std::cout;
using std::endl;

int main() {
	EmployeEtudiantULB membre( "Dupont", "Laurent", "IRCI3-B", "jobiste" );
	membre.print(); cout << endl;
	membre.EtudiantULB::print(); cout << endl;
	membre.EmployeULB::print(); cout << endl;
	cout << "Fin du programme" << endl;
	return 0;
}
