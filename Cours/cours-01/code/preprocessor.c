#include <stdio.h>
#include "decl.h"
#define PI 3.14159265
#define square(x) (x)*(x)

int main()
{
    printf("Le carré de %i vaut %i\n", 2,square(1+1));
    return 0;
}
