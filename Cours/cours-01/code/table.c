/* table: imprime une table de multiplication */
int table(int a)
{
	int produit, facteur = 0;
	while ( facteur<10 )
	{
		produit = a*(++facteur);
		printf("%i fois %i egale %i\n", a, facteur, produit);
	}
	return 0;
}

int main()
{
	...
	return 0;
}
