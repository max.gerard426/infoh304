# Cours 01 - Introduction - Langage C (1)

- Vidéo d'introduction: [Lien](https://universitelibrebruxelles-my.sharepoint.com/:v:/r/personal/jeremie_roland_ulb_be/Documents/INFOH304/Cours%20th%C3%A9oriques/Cours%2001a%20-%20Introduction%20au%20cours.mp4?csf=1&web=1&e=xf8csc)
	- Machine virtuelle
	- Organisation du cours
	- Références
- Langage C (1)
	- Introduction
	- Syntaxe
